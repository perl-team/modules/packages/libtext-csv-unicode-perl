Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Text-CSV-Unicode
Upstream-Contact: Robin Barker <rmbarker@cpan.org>
Source: https://metacpan.org/release/Text-CSV-Unicode

Files: *
Copyright: 2007, 2008, 2010-2012, 2018, Robin Barker <rmbarker@cpan.org>
License: Artistic or GPL-1+
Comment:
 From README:
    lib/Text/CSV/Base.pm (version 0.01) was a direct copy of
    Text-CSV-0.01/CSV.pm, and t/base.t was a direct copy of
    Text-CSV-0.01/test.pl, with only the module name changed
    (and white-space changes), and these files are
    Copyright (c) 1997 Alan Citterman.
 .
 Text::CSV is also licensed as Artistic or GPL-1+
 (Cf. libtext-csv-perl)

Files: debian/*
Copyright: 2022-2023, Mason James <mtj@kohaaloha.com>
License: Artistic or GPL-1+

License: Artistic
 This program is free software; you can redistribute it and/or modify
 it under the terms of the Artistic License, which comes with Perl.
 .
 On Debian systems, the complete text of the Artistic License can be
 found in `/usr/share/common-licenses/Artistic'.

License: GPL-1+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 1, or (at your option)
 any later version.
 .
 On Debian systems, the complete text of version 1 of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-1'.
